#!/usr/bin/env bash

sudo apt update
sudo apt install software-properties-common python3-pip python3-neovim
pip3 install ansible --user

echo 'export PATH="$PATH:/home/$USER/.local/bin"' >> ~/.profile
source ~/.profile

ansible-playbook -vv -K -c local playbook/main.yml
